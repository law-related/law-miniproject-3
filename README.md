## Vincentius Adi Kurnianto - 1706979480


## How to run the app
- Clone the repository
```cmd
git clone https://gitlab.com/law-related/law-miniproject-3.git
```

- Run the app
```cmd
docker-compose up -d --build
```

- Open **http://localhost:5000/**

- Stop the app
```cmd
docker-compose down -v
```