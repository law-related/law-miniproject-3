import threading
import grpc
import string,secrets,requests,time,os,pika,urllib3,certifi
from concurrent import futures
import miniproject3_pb2 as pb2
import miniproject3_pb2_grpc as pb2_grpc
exchange= "1706979480"
tes_url = "https://www.pixsy.com/wp-content/uploads/2021/04/ben-sweet-2LowviVHZ-E-unsplash-1.jpeg"

def generate_routing_key(length):
    return ''.join(secrets.choice(string.ascii_letters + string.digits) for x in range(length))

def download_service(url,routing_key,exchange_name):

    print("here")
    file_name = url.split('/')[-1]
    http = urllib3.PoolManager(
        cert_reqs='CERT_REQUIRED',
        ca_certs=certifi.where()

    )


    r = http.request('GET', url, preload_content=False)

    file_size = int(r.headers["Content-Length"])


    print("Downloading: {} Bytes: {}".format(file_name, file_size))

    file_size_dl = 0
    block_sz = 8192
    file_path = os.path.join("downloaded",file_name)
    f = open(file_path, "wb")
    while True:
        buffer = r.read(block_sz)
        if not buffer:
            break
        file_size_dl += len(buffer)
        f.write(buffer)
        percentage = "{}".format(int(file_size_dl * 100. // file_size))
        percentage = percentage + chr(8)*(len(percentage)+1)
        publish_progress(percentage,routing_key,exchange_name)
        print("data:" + str(percentage))
    print('finished!')
    f.close()
    final_message = 'http://localhost:5000/' + file_path
    publish_progress(final_message,routing_key,exchange_name)




def publish_progress(percentage, routing_key, exchange_name=exchange):
    print(percentage, flush=True)
    connection = pika.BlockingConnection(
        pika.URLParameters('amqp://guest:guest@rabbitmq')
    )
    channel = connection.channel()
    channel.exchange_declare(exchange=exchange_name, exchange_type="direct")
    channel.basic_publish(
        exchange=exchange_name,
        routing_key=routing_key,
        body=bytes(str(percentage), encoding="UTF-8"),
        mandatory=True,
    )

def async_download_service(url,routing_key,exchange=exchange):
    # filename = url.split("/")[-1]
    # file_path = os.path.join('downloaded', '', filename)
    pass



class MiniProject3Servicer(pb2_grpc.MiniProjectServiceServicer):
    def Download(self, request, context):
        url = request.url
        routing_key = generate_routing_key(20)
        thread = threading.Thread(target=download_service, args=(url, routing_key, exchange))
        thread.start()
        # async_download_service(url,routing_key)
        response = pb2.DownloadResponse()
        response.url = url
        response.uniq_id = routing_key
        return response







def main():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    pb2_grpc.add_MiniProjectServiceServicer_to_server(MiniProject3Servicer(),server)
    # download_service()
    server.add_insecure_port('[::]:50051')
    server.start()
    try:
        while True:
            time.sleep(86400)

    except KeyboardInterrupt:
        server.stop(0)


if __name__=="__main__":
    main()

