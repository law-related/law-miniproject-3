from flask import Flask,render_template,request,send_from_directory
import grpc,os
import miniproject3_pb2 as pb2
import miniproject3_pb2_grpc as pb2_grpc

app = Flask(__name__)

exchange = "1706979480"

HTTP_METHODS = ['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE', 'PATCH']

os.makedirs('downloaded', exist_ok=True)
def execute_with_grpc(url):
    channel = grpc.insecure_channel(
            '{}:{}'.format('agent', '50051'))
    stub = pb2_grpc.MiniProjectServiceStub(channel=channel)
    response = None

    try:
        response = stub.Download(pb2.DownloadRequest(url=url))
    except:
        response = "Error, something wrong!"
    return response






@app.route('/',methods=HTTP_METHODS)
def hello_world():
    response = {}
    if request.method=="POST":
        file_url = request.form['url']
        response_grpc = execute_with_grpc(file_url)
        response = {
            'url':response_grpc.url,
            'routing_key':response_grpc.uniq_id,
            'exchange_name':exchange,
        }
        print('responsenya: ',response)
        return render_template("index.html", **response)
    return render_template("index.html")

@app.route('/downloaded/<filename>')
def upload(filename):
    return send_from_directory('downloaded', filename, as_attachment=True)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
